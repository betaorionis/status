from django.shortcuts import redirect, render
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User

# Create your views here.

def signup(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if(not username or not password):
            messages.error(request, "Complete your data first.")
            return render(request, 'signup.html')

        if User.objects.filter(username__iexact=username).exists():
            messages.error(request, "Username already exist.")
            return render(request, 'signup.html')
        else:
            user = User.objects.create_user(username=username, password=password)
            user.save()

            messages.success(request, "Successfully signed in.")

            user_auth = authenticate(request, username=username, password=password)
            login(request, user_auth)

            return redirect('/user/')
    else:
        return render(request, 'signup.html')


def login_page(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        if(not username or not password):
            messages.error(request, "Complete your data first.")
            return render(request, 'login.html')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/user/')
        else:
            print("gagal")
            messages.error(request, "You sure that's correct?")
            return render(request, 'login.html')
    
    else:      
        return render(request, 'login.html')


def home(request):
    return render(request, 'homepage.html')


def logout_oauth(request):
    logout(request)
    messages.success(request, "Successfully logged out.")
    return redirect('/user/')