from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver

from django.apps import apps
from .apps import AboutConfig
from .views import about
# Create your tests here.

class story8UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(AboutConfig.name, 'about')
        self.assertEqual(apps.get_app_config('about').name, 'about')

    def test_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)
    
    def test_about_function(self):
        self.found = resolve('/about/')
        self.assertEqual(self.found.func, about)
    
    def test_intro_is_exist(self):
        response = Client().get('/about/')
        self.assertIn('<h2>a little about myself</h2>', response.content.decode())
    
    def test_accordion_is_exist(self):
        self.response = Client().get('/about/')
        self.assertIn('<div id="accordion">', self.response.content.decode())

    def test_accordion_header_is_exist(self):
        self.response = Client().get('/about/')
        self.assertIn('<div class="info acc-head">', self.response.content.decode())

    def test_accordion_content_is_exist(self):
        self.response = Client().get('/about/')
        self.assertIn('<div class="content">', self.response.content.decode())


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_accordion(self):
        # open up the web page
        self.browser.get(self.live_server_url + '/about/')
        time.sleep(5)

        # expand personal info
        info = self.browser.find_element_by_class_name('info')
        info.click()
        time.sleep(2)

        contain = self.browser.find_element_by_tag_name('body').text
        self.assertIn('betaorionis', contain)
        # expand activity
        activity = self.browser.find_element_by_class_name('activity')
        activity.click()
        time.sleep(2)

        cont_act = self.browser.find_element_by_tag_name('body').text
        self.assertIn('nugas', cont_act)
        
        # expand experience
        experience = self.browser.find_element_by_class_name('experience')
        experience.click()
        time.sleep(2)
        
        cont_exp = self.browser.find_element_by_tag_name('body').text
        self.assertIn('kepeleset', cont_exp)
        
        # expand achievement
        achievement = self.browser.find_element_by_class_name('achievement')
        achievement.click()
        time.sleep(2)
        
        contain_pers = self.browser.find_element_by_tag_name('body').text
        self.assertIn('a', contain_pers)
        
    
        # down button Personal Info
        down = self.browser.find_element_by_class_name('down-info')
        down.click()
        
        page_text = self.browser.find_element_by_tag_name('body').text
        # self.assertTrue(page_text.find('Activity') < page_text.find('Personal Information'))


        # up button experience accordion
        experience.click()
        up = self.browser.find_element_by_class_name('up-exp')
        up.click()
        page_text = self.browser.find_element_by_tag_name('body').text
        # self.assertTrue(page_text.find('Experience') < page_text.find('Personal Information'))

    def test_dark_theme(self):
        self.browser.get(self.live_server_url + '/about/')
        time.sleep(5)
        dark_theme_button = self.browser.find_element_by_id('dark_theme')
        dark_theme_button.send_keys(Keys.RETURN)
        time.sleep(10)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(52, 58, 64, 1)')
        self.tearDown()

    def test_dark_theme_hover(self):
        self.browser.get(self.live_server_url + '/about/')
        time.sleep(5)
        action = ActionChains(self.browser)
        dark_theme_button = self.browser.find_element_by_id('dark_theme')
        action.move_to_element(dark_theme_button).perform()
        time.sleep(10)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(52, 58, 64, 1)')
        self.tearDown()

    def test_light_theme(self):
        self.browser.get(self.live_server_url + '/about/')
        time.sleep(5)
        light_theme_button = self.browser.find_element_by_id('light_theme')
        light_theme_button.send_keys(Keys.RETURN)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(240, 248, 255, 1)')
        self.tearDown()

    def test_light_theme_hover(self):
        self.browser.get(self.live_server_url + '/about/')
        time.sleep(5)
        action = ActionChains(self.browser)
        light_theme_button = self.browser.find_element_by_id('light_theme')
        action.move_to_element(light_theme_button).perform()
        time.sleep(5)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(240, 248, 255, 1)')
        self.tearDown()
