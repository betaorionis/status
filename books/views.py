from django.shortcuts import render
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
from django.forms.models import model_to_dict
import requests
from json import loads
from .models import Books

def books(request):
    return render(request, 'books.html')

def get_data(request):
    if request.method == "POST":
        idBuku = request.POST.get('id_buku')
        url = "https://www.googleapis.com/books/v1/volumes?q=" + idBuku
        books_json = requests.get(url).json().get("items")[0]
        try:
            obj = Books.objects.get(id_buku = idBuku)
            obj.jumlah_like += 1
            obj.save()
        except Books.DoesNotExist:
            img = books_json.get('volumeInfo').get('imageLinks', None)
            if (img): img = img.get("smallThumbnail", "")
            obj = Books(
                id_buku = idBuku,
                cover = img,
                title = books_json.get("volumeInfo").get("title"),
                authors = books_json.get("volumeInfo").get("authors")[0],
                jumlah_like = 1
            )
            obj.save()
        context = [{"likes": obj.jumlah_like}]

        return JsonResponse({"items" : context})


    else:
        url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('keyword')
        books_json = requests.get(url).json()
        lst_books = []

        for book in books_json.get("items"):

            try:
                obj = Books.objects.get(id_buku = book["id"])
                temp = {
                    "id" : obj.id_buku,
                    "cover" : obj.cover,
                    "title" : obj.title,
                    "authors" : obj.authors,
                    "likes" : obj.jumlah_like
                }

            except Books.DoesNotExist:
                img = book.get('volumeInfo').get('imageLinks', None)
                if (img): img = img.get("smallThumbnail", "")

                temp = {
                    "id" : book.get("id"),
                    "cover" : img,
                    "title" : book.get("volumeInfo").get("title"),
                    "authors" : book.get("volumeInfo").get("authors"),
                    "likes" : 0
                }
            lst_books.append(temp)
        
        return JsonResponse({"items" : lst_books})

def top_books(request):
    if request.method == "GET":
        books = Books.objects.all()[:5]
        lst_books = []
        for i in books:
            temp = {
                'id' : i.id_buku,
                'cover' : i.cover,
                'title' : i.title,
                'authors' : i.authors,
                'likes' : i.jumlah_like
            }
            lst_books.append(temp)

        return JsonResponse({"items" : lst_books})