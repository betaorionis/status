from django.urls import path
from . import views

app_name = 'status'

urlpatterns = [
    path('', views.status, name='home'),
    path('confirm/', views.konfirmasi, name='confirm'),
    path('color/<int:index>/', views.colorChange)
]